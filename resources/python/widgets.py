import json
import os
import boto3

def getTable():
    dynamodb = boto3.resource('dynamodb')
    table_name:str = os.environ["TABLE"]
    return dynamodb.Table(table_name)

def get_body(event, function_name:str)->str:
    return json.dumps({
        "message": f"This is the {function_name}.",
        "environ": {**os.environ},
        "event": {**event}
    })

def widget_reader(event, _):
    row_id:str = event["pathParameters"]["id"]
    item = getTable().get_item(Key={'id': row_id})


    return {
        "statusCode": 200,
        "headers": {},
        "body": json.dumps(item, function_name="Reader"),
    }

def widget_writer(event, _):
    row_id:str = event["pathParameters"]["id"]
    items = {"id":row_id, **(event["queryStringParameters"] or {})}
    getTable().put_item(Item=items)

    return {
        "statusCode": 201,
        "headers": {},
        "body": get_body(event=event, function_name="Writer")
    }

def widget_deleter(event, _):
    row_id:str = event["pathParameters"]["id"]
    getTable().delete_item(Key={"id":row_id})

    return {
        "statusCode": 204,
        "headers": {},
        "body": get_body(event=event, function_name="Deleter")
    }

