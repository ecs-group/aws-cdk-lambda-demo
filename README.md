# CDK Lambda Example

#### Running The Example
```
mvn clean install -DskipTests
cdk synth
cdk deploy
cdk destroy
```

#### Potential Errors
If you get the following error 
```
❌  WidgetServiceStack failed: Error: This stack uses assets, so the toolkit stack must be deployed to the environment (Run "cdk bootstrap aws://unknown-account/unknown-region")
```

run the following command.
```
cdk bootstrap aws://unknown-account/unknown-region
```

## Useful commands

 * `mvn package`     compile and run tests
 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation