package com.ecs.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Duration;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.services.apigateway.LambdaIntegration;
import software.amazon.awscdk.services.apigateway.Resource;
import software.amazon.awscdk.services.apigateway.RestApi;
import software.amazon.awscdk.services.dynamodb.Attribute;
import software.amazon.awscdk.services.dynamodb.AttributeType;
import software.amazon.awscdk.services.iam.IManagedPolicy;
import software.amazon.awscdk.services.iam.ManagedPolicy;
import software.amazon.awscdk.services.iam.Role;
import software.amazon.awscdk.services.iam.ServicePrincipal;
import software.amazon.awscdk.services.lambda.Code;
import software.amazon.awscdk.services.lambda.Function;
import software.amazon.awscdk.services.lambda.Runtime;
import software.amazon.awscdk.services.s3.Bucket;
import software.amazon.awscdk.services.dynamodb.Table;

public class MyWidgetServiceStack extends Stack {

  private Table getTable() {
    return Table.Builder.create(this, "WidgetTable").partitionKey(Attribute.builder().name("id").type(AttributeType.STRING).build()).build();
  }

  public Function lambdaFactory(Map<String,String> environmentVariables, String handlerName, String handlerId) {
    return Function.Builder.create(this, handlerId)
            .code(Code.fromAsset("resources/python"))
            .handler(handlerName)
            .timeout(Duration.seconds(20))
            .runtime(Runtime.PYTHON_3_8)
            .environment(environmentVariables)
            .build();
  }

  public MyWidgetServiceStack(final Construct scope, final String id) {
    super(scope, id, null);

    Table widgetTable = getTable();
    Bucket bucket = Bucket.Builder.create(this, "WidgetStore").build();

    RestApi api =
        RestApi.Builder.create(this, "widgets-api")
            .restApiName("Widget Service")
            .description("This service serves widgets.")
            .build();

    List<IManagedPolicy> managedPolicyArray = new ArrayList<IManagedPolicy>();
    managedPolicyArray.add(
        (IManagedPolicy) ManagedPolicy.fromAwsManagedPolicyName("AmazonS3FullAccess"));

    Role restApiRole =
        Role.Builder.create(this, "RestAPIRole")
            .assumedBy(new ServicePrincipal("apigateway.amazonaws.com"))
            .managedPolicies(managedPolicyArray)
            .build();

    Map<String, String> environmentVariables = new HashMap<String, String>();
    environmentVariables.put("BUCKET", bucket.getBucketName());
    environmentVariables.put("TABLE", widgetTable.getTableName());

    Function widgetReader = lambdaFactory(environmentVariables, "widgets.widget_reader", "widgetReader");
    Function widgetWriter = lambdaFactory(environmentVariables, "widgets.widget_writer", "widgetWriter");
    Function widgetDeleter = lambdaFactory(environmentVariables, "widgets.widget_deleter", "widgetDeleter");

    bucket.grantRead(widgetReader);
    bucket.grantReadWrite(widgetWriter);
    bucket.grantDelete(widgetDeleter);

    widgetTable.grantReadData(widgetReader);
    widgetTable.grantReadWriteData(widgetWriter);
    widgetTable.grantWriteData(widgetDeleter);




    Map<String, String> lambdaIntegrationMap = new HashMap<String, String>();
    lambdaIntegrationMap.put("application/json", "{ \"statusCode\": \"200\" }");

    LambdaIntegration getWidgetReaderIntegration =
        LambdaIntegration.Builder.create(widgetReader)
            .requestTemplates(lambdaIntegrationMap)
            .build();

    api.getRoot().addMethod("GET", getWidgetReaderIntegration);

    LambdaIntegration postWidgetIntegration = new LambdaIntegration(widgetWriter);
    LambdaIntegration deleteWidgetIntegration = new LambdaIntegration(widgetDeleter);

    Resource widget = api.getRoot().addResource("{id}");

    widget.addMethod("POST", postWidgetIntegration);
    widget.addMethod("GET", getWidgetReaderIntegration);
    widget.addMethod("DELETE", deleteWidgetIntegration);
  }
}
